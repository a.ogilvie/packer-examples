## [1.1.0] - 2021-04-08
### Added
- os-centos-7/README.md for clarity.

## [1.0.0] - 2021-04-08
- Initial commit.

[1.1.0]: https://gitlab.com/a.ogilvie/packer-examples/-/tags/v1.1.0
[1.0.0]: https://gitlab.com/a.ogilvie/packer-examples/-/tags/v1.0.0

