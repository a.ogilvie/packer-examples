Packer Examples
---------------

This repository contains examples for building automated machine 
images, using Packer, Kickstart, and Ansible.

In `anaconda.cfg`, the root password can be created as follows (this 
will generate a sha512 crypt-compatible hash of your password using a 
random salt):

```sh
$ python -c 'import crypt,getpass;pw=getpass.getpass();print(crypt.crypt(pw) if (pw==getpass.getpass("Confirm: ")) else exit())'
```

