Instructions for building a CentOS 7 KVM virtual image using the 
Packer QEMU builder
----------------------------------------------------------------

## Tested on:

* CentOS CentOS-7.9.2009 (Core)
* CentOS-7-x86_64-Minimal-2009.iso
* Packer 1.7.0

## Dependencies:

* KVM
* Packer

## Instructions:

Edit `packer.json` as needed, then run as follows:

```sh
$ packer build packer.json
```

Things to keep in mind:

* In `packer.json`, the `pause_before_connecting` directive needs to 
  be set sufficiently long enough for both the OS to be installed and 
  for Kickstart to do a reboot afterwards into the post-installation
  environment. 

* Since I'm running yum commands inside the `shell` provisioner 
  (see the end of `packer.json`), these commands will only work 
  in the post-installation environment, not during the 
  chroot/installation environment.

* Setting the `pause_before_connecting` value too low will result in 
  Packer connecting too soon (i.e., to the chroot/installation 
  environment instead of the post-installation environment). The 
  setting that worked for me was "6m", but this may need to be adjusted   to account for different environments.

